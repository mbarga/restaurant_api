# Sample CRUD API
[![pipeline status](https://gitlab.com/mbarga/restaurant_api/badges/master/pipeline.svg)](https://gitlab.com/mbarga/restaurant_api/commits/master) 
[![coverage report](https://gitlab.com/mbarga/restaurant_api/badges/master/coverage.svg)](https://gitlab.com/mbarga/restaurant_api/commits/master)

## Getting started
Clone the application or have the project folder present on your local workspace.

`git clone https://gitlab.com/mbarga/restaurant_api.git`

There are two options for running this project:

1. Manual setup using pip/virtualenv
2. Automated setup using Docker

### Using Pip/Virtualenv

Create a virtual environment in the project folder and activate it:

```
cd ./restaurant_api
```
```
virtualenv -p python3 venv
```
```
source ./venv/bin/activate
```
```
pip install -r requirements.txt
```
```
fab init_db
```
```
fab run_server
```

### Using Docker

```
cd ./restaurant_api
```
```
docker build -t crud-app .
```
```
docker run -it -p 8080:8080 crud-app     
```

## Using API

This API has a restaurant endpoint that deals with Restaurant objects with the following associated data:

**id**: integer  
**name**: string
  * max length 100  

**opens_at**: datetime   
  * can be null  
  * passed as an integer between 0 and 24  

**closes_at**: datetime   
  * can be null  
  * passed as an integer between 0 and 24  

The following endpoints are supported:

### POST
**/restaurant/** creates a restaurant item given the following input:
```
{
  "name": "Pizza Pie",
  "opens_at": "10",
  "closes_at": "17"
}
```
```
curl -d "name=Pizza Pie&opens_at=10&closes_at=17" -X POST http://0.0.0.0:8080/restaurant/
```

### GET
**/restaurant/** gets a list of stored restaurant items (limited to 10)  
**/restaurant/:id** gets restaurant with ID of 1 
```
curl http://0.0.0.0:8080/restaurant/
```

### PUT
**/restaurant/:id** updates the restaurant object with specified ID

```
curl -d "name=Baked Burgers&opens_at=9&closes_at=20" -X PUT http://0.0.0.0:8080/restaurant/1
```

### DELETE
**/restaurant/:id** deletes the restaurant item with specified ID

```
curl -X DELETE http://0.0.0.0:8080/restaurant/1
```


## Running tests

To run unit tests and code coverage, refer to the fabfile.

```
fab run_tests
```
```
fab coverage
```


## Tools used
* Flask
  - microframework providing structure for API endpoints
* Flask_Migrate
  - SQL database migration
* SQLAlchemy
  - ORM for Flask
* fabric3
  - define common tasks and actions in one place
* coverage
  - code coverage for python tests
* gitlab continuous integration pipeline

    
## Not implemented
* documentation
  - sphinx
* asyncronous processing
  - asyncio
* production deployment
  - web server/WSGI
* separate environments
  - production/staging/dev environments
* logging
* error handling
  - custom 404, etc. 
* caching
  - redis/memcache if necessary (for example commonly requested data)

FROM python:3.6

# copy and set working directory
ADD . /rest_app
WORKDIR /rest_app
# install pip requirements
RUN pip install -r requirements.txt
# set environment variables
ENV PYTHONPATH=.
ENV FLASK_APP=./restaurant_api/app.py
# initialize the database
RUN flask db init
RUN flask db migrate
RUN flask db upgrade
# application startup
ENTRYPOINT [ "python", "run.py" ]

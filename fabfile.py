# -*- coding: UTF-8 -*-

import os

from fabric.api import local, cd, shell_env

"""Encode common functions for project management
"""


def run_server():
    with shell_env(PYTHONPATH='.', FLASK_APP='./restaurant_api/app.py'):
        local("python run.py")


def run_tests():
    with shell_env(PYTHONPATH='.', FLASK_APP='./restaurant_api/app.py'):
        local("python ./restaurant_api/tests/mytests.py")


def coverage():
    with shell_env(PYTHONPATH='.', FLASK_APP='./restaurant_api/app.py'):
        local("coverage run --source ./restaurant_api ./restaurant_api/tests/mytests.py")
        local("coverage report")


def init_db():
    with cd("./restaurant_api"):
        if os.path.exists('app.db'):
            local("rm app.db")
        if os.path.isdir("./migrations"):
            local("rm -rf ./migrations")
    with shell_env(PYTHONPATH='.', FLASK_APP='./restaurant_api/app.py'):
        local("flask db init")
        local("flask db migrate")
        local("flask db upgrade")

# TODO: Include scripts for ssh, deployment, etc.

# def deploy():

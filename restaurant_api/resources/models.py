# -*- coding: UTF-8 -*-

import datetime

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

"""This file contains the Restaurant schema definition using SQLAlchemy ORM.
"""


class Restaurant(db.Model):
    """restaurant table"""

    __tablename__ = 'restaurants'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), index=True, nullable=False)
    opens_at = db.Column(db.DateTime)
    closes_at = db.Column(db.DateTime)

    def __init__(self, name, opens_at=None, closes_at=None):
        self.name = name
        self.opens_at = datetime.datetime(1900, 1, 1, opens_at, 0, 0)
        self.closes_at = datetime.datetime(1900, 1, 1, closes_at, 0, 0)

    @property
    def serialize(self):
        """serialized restaurant object"""
        return dict(
            id=self.id,
            name=self.name,
            opens_at=self.__date_to_str(self.opens_at),
            closes_at=self.__date_to_str(self.closes_at)
        )

    def __date_to_str(self, date):
        """convert datetime to time string"""
        if date is None:
            return None
        else:
            return date.strftime("%H:%M")

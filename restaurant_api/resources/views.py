# -*- coding: UTF-8 -*-

import datetime

from flask import request, jsonify
from flask.views import MethodView

from restaurant_api import app
from .models import db, Restaurant

"""I have chosen to implement class based pluggable views for the API rather than functional views.
Class based views can give more structure and organization to the the views especially for CRUD applications
where a common set of actions are defined for modifying database objects. The use of flask MethodView
also makes the code more readable by defining get(), post(), etc. actions clearly for each class. In this way,
separate classes can be defined for each database object that encapsulate all CRUD operations.
"""


class RestaurantAPI(MethodView):
    """get a restaurant object if ID was passed, else return all resturant objects"""

    def get(self, id):
        if id is None:
            # limit to 10 records
            restaurants = Restaurant.query.limit(10).all()
            return jsonify([restaurant.serialize for restaurant in restaurants])
        else:
            restaurant = Restaurant.query.get(id)
            if restaurant is None:
                return jsonify(dict(status='error', message='Restaurant does not exist in DB.'))
            return jsonify(restaurant.serialize)

    """create a new restaurant object"""

    def post(self):
        name = request.form.get('name')
        opens_at = request.form.get('opens_at')
        closes_at = request.form.get('closes_at')
        # validate parameter data
        if opens_at is not None:
            opens_at = int(opens_at)
            if opens_at < 0 or opens_at > 24:
                return jsonify(dict(status='error', message='Format of business hours was incorrect'))
        if closes_at is not None:
            closes_at = int(closes_at)
            if closes_at < 0 or closes_at > 24:
                return jsonify(dict(status='error', message='Format of business hours was incorrect'))
        if len(name) > 100:
            return jsonify(dict(status='error', message='Name must be 100 characters or less'))
        if name is None:
            return jsonify(dict(status='error', message='Name cannot be Null'))

        restaurant = Restaurant(name, opens_at, closes_at)
        db.session.add(restaurant)
        try:
            db.session.commit()
            return jsonify(restaurant.serialize)
        except:
            db.session.rollback()
            return jsonify(dict(status='error', message='SQLALchemy error during commit!'))

    """delete a restaurant object"""

    def delete(self, id):
        # make sure ID is present
        if id is None:
            return jsonify(dict(status='error', message='Please provide an ID'))

        restaurant = Restaurant.query.get(id)
        if restaurant is None:
            return jsonify(dict(status='error', message='Restaurant does not exist in DB.'))
        db.session.delete(restaurant)
        try:
            db.session.commit()
            return jsonify(dict(status='success', message='Restaurant ID: ' + str(id) + ' was deleted.'))
        except:
            db.session.rollback()
            return jsonify(dict(status='error', message='SQLALchemy error during commit!'))

    """update a restaurant object"""

    def put(self, id):
        # make sure ID is present
        if id is None:
            return jsonify(dict(status='error', message='Please provide an ID'))

        name = request.form.get('name')
        opens_at = request.form.get('opens_at')
        closes_at = request.form.get('closes_at')
        # validate parameter data
        if opens_at is not None:
            opens_at = int(opens_at)
            if opens_at < 0 or opens_at > 24:
                return jsonify(dict(status='error', message='Format of business hours was incorrect'))
        if closes_at is not None:
            closes_at = int(closes_at)
            if closes_at < 0 or closes_at > 24:
                return jsonify(dict(status='error', message='Format of business hours was incorrect'))
        if len(name) > 100:
            return jsonify(dict(status='error', message='Name must be 100 characters or less'))
        if name is None:
            return jsonify(dict(status='error', message='Name cannot be Null'))

        restaurant = Restaurant.query.get(id)
        if restaurant is None:
            return jsonify(dict(status='error', message='Restaurant does not exist in DB.'))
        restaurant.name = name
        restaurant.opens_at = datetime.datetime(1900, 1, 1, opens_at, 0, 0)
        restaurant.closes_at = datetime.datetime(1900, 1, 1, closes_at, 0, 0)
        try:
            db.session.commit()
            return jsonify(restaurant.serialize)
        except Exception as e:
            db.session.rollback()
            return jsonify(dict(status='error', message='SQLALchemy error during commit!'))


restaurant_view = RestaurantAPI.as_view('restaurant_api')
app.add_url_rule('/restaurant/', defaults={'id': None}, view_func=restaurant_view, methods=['GET', ])
app.add_url_rule('/restaurant/', view_func=restaurant_view, methods=['POST', ])
app.add_url_rule('/restaurant/<int:id>', view_func=restaurant_view, methods=['GET', 'PUT', 'DELETE'])

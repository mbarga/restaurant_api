# -*- coding: UTF-8 -*-

import os

from flask_migrate import Migrate

from restaurant_api import app

"""This file sets up the restaurant API app configuration and ties the SQLAlchemy database to the application context.
For this project I use a local sqlite database store for portability, but in a real project I would define a separate
database and register the URI here.
"""

# set to avoid warning
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# configure the database to use local sqlite store
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')

# initialize app context for database
from restaurant_api.resources.models import db

db.init_app(app)

# define migration for flask-migrate
migrate = Migrate(app, db)

# -*- coding: UTF-8 -*-

import os
import unittest

from restaurant_api import app
from restaurant_api.resources.models import db, Restaurant

"""Unittests were built using Python built-in unittesting framework for simplicity.
These tests are not comprehensive, but I want to show that unittests should always be a part of project development.
"""


class TestRestaurantCase(unittest.TestCase):
    """initialize database and create flask application context"""

    def setUp(self):
        self.app = self._create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        db.init_app(self.app)
        db.create_all()

    """close database and destroy flask application"""

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    """create flask application"""

    def _create_app(self):
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.config['TESTING'] = True
        basedir = os.path.abspath(os.path.dirname(__file__))
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        return app

    """test that database was created and no records present"""

    def test_empty_db(self):
        assert self.app is not None
        resp = self.client.get('/restaurant/')
        assert resp.status_code == 200
        assert "[]" in resp.data.decode("utf8")

    """test for restaurant object creation"""

    def test_restaurant_post_success(self):
        assert self.app is not None
        args = dict(
            name="Berlin Burgers",
            opens_at=10,
            closes_at=17
        )
        resp = self.client.post('/restaurant/', data=args)
        assert resp.status_code == 200
        assert "Berlin Burgers" in resp.data.decode("utf8")

    def test_restaurant_post_error(self):
        assert self.app is not None
        args = dict(
            name="Berlin Burgers",
            opens_at=100,
        )
        resp = self.client.post('/restaurant/', data=args)
        assert resp.status_code == 200
        assert "Format of business hours was incorrect" in resp.data.decode("utf8")

    """test ability to retrieve restaurant data"""

    def test_restaurant_get(self):
        assert self.app is not None
        restaurant = Restaurant('Berlin Pizza', 10, 17)
        db.session.add(restaurant)
        db.session.commit()

        # check with no ID present
        resp = self.client.get('/restaurant/')
        assert resp.status_code == 200
        assert "Berlin Pizza" in resp.data.decode("utf8")

        # check call with ID
        resp = self.client.get('/restaurant/1')
        assert resp.status_code == 200
        assert "Berlin Pizza" in resp.data.decode("utf8")

    # TODO: more tests to achieve better code coverage...


if __name__ == "__main__":
    unittest.main()
